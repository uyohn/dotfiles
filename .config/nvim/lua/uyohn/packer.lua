--init This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- Workflow
    use {
        'nvim-telescope/telescope.nvim', tag = '0.1.6',
        requires = { { 'nvim-lua/plenary.nvim' } }
    }
    use('mbbill/undotree')

    -- -- file tree
    use {
        "nvim-neo-tree/neo-tree.nvim",
        branch = "v2.x",
        requires = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons",
            "MunifTanjim/nui.nvim",
        }
    }

    -- -- status line
    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'nvim-tree/nvim-web-devicons', opt = true }
    }

    -- -- better folds
    use {
        'kevinhwang91/nvim-ufo',
        requires = {
            'kevinhwang91/promise-async',
            'luukvbaal/statuscol.nvim',
        }
    }

    -- -- floating terminal
    use "numToStr/FTerm.nvim"

    -- -- commenting
    use('numToStr/Comment.nvim')

    -- -- git signs
    use('lewis6991/gitsigns.nvim')

    -- -- buffer line
    use { 'akinsho/bufferline.nvim', requires = 'nvim-tree/nvim-web-devicons' }

    -- -- delete buffers without messing up layout
    use 'famiu/bufdelete.nvim'

    -- Cosmetics
    use({
        'rose-pine/neovim',
        as = 'rose-pine',
        config = function()
            require('rose-pine').setup()
        end
    })

    use {
        "loctvl842/monokai-pro.nvim",
        config = function()
            require("monokai-pro").setup()
            vim.cmd('colorscheme monokai-pro')
        end
    }

    -- Syntax, LSP & DAP
    use('nvim-treesitter/nvim-treesitter', { run = ':TSUpdate' })
    use('nvim-treesitter/playground')
    use('github/copilot.vim')
    use('tpope/vim-fugitive')

    -- -- lsp
    use {
        'VonHeikemen/lsp-zero.nvim',
        requires = {
            -- LSP Support
            { 'neovim/nvim-lspconfig' },
            { 'williamboman/mason.nvim' },
            { 'williamboman/mason-lspconfig.nvim' },

            -- Autocompletion
            { 'hrsh7th/nvim-cmp' },
            { 'hrsh7th/cmp-buffer' },
            { 'hrsh7th/cmp-path' },
            { 'saadparwaiz1/cmp_luasnip' },
            { 'hrsh7th/cmp-nvim-lsp' },
            { 'hrsh7th/cmp-nvim-lua' },

            -- Snippets
            { 'L3MON4D3/LuaSnip' },
            { 'rafamadriz/friendly-snippets' },
        }
    }

    -- -- dap - language specific adapters needed, install using mason
    use {
        'mfussenegger/nvim-dap',                 -- base dap support
        requires = {
            'theHamsta/nvim-dap-virtual-text',   -- text in code
            'nvim-telescope/telescope-dap.nvim', -- telescope integration
            'rcarriga/nvim-dap-ui',              -- ui
            'nvim-neotest/nvim-nio',             -- async io
            'jay-babu/mason-nvim-dap.nvim',      -- mason support
        }
    }

    -- -- lint
    use('mfussenegger/nvim-lint')

    -- ctags
    use "c0r73x/neotags.lua"
end)
