vim.g.mapleader = " "

-- split navigation
vim.keymap.set("n", "<C-h>", "<C-w>h", { desc = "Switch to pane that's left of the current one." })
vim.keymap.set("n", "<C-j>", "<C-w>j", { desc = "Switch to pane that's down of the current one." })
vim.keymap.set("n", "<C-k>", "<C-w>k", { desc = "Switch to pane that's up of the current one." })
vim.keymap.set("n", "<C-l>", "<C-w>l", { desc = "Switch to pane that's right of the current one." })


vim.keymap.set("n", "<leader>q", vim.cmd.bdelete, { desc = "Delete current buffer." })

-- move highlighted lines up and down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv", { desc = "Move selected lines down." })
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv", { desc = "Move selected lines up." })

-- useless? Combine lines...
vim.keymap.set("n", "J", "mzJ`z", { desc = "Combine current line with the next one." })

-- stay cenered when page d/u
vim.keymap.set("n", "<C-d>", "<C-d>zz", { desc = "Stay centered horizontally when page down." })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { desc = "Stay centered horizontally when page up." })
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- greatest remap ever
vim.keymap.set("x", "<leader>p", "\"_dP", { desc = "Delete selected text and paste yanked text in it's place." })
vim.keymap.set("x", "<leader>P", "\"_d\"+P", { desc = "Delete selected text and paste clipboard in it's place." })

-- next greatest remap ever : asbjornHaland
vim.keymap.set("n", "<leader>y", "\"+y", { desc = "Copy to computer's clipboard." })
vim.keymap.set("v", "<leader>y", "\"+y", { desc = "Copy to computer's clipboard." })
vim.keymap.set("n", "<leader>Y", "\"+Y", { desc = "Copy to computer's clipboard." })

-- This is going to get me cancelled
-- vim.keymap.set("i", "<C-c>", "<Esc>") -- not sure I need this

vim.keymap.set("n", "Q", "<nop>", { desc = "Ignore Q keymap." })
vim.keymap.set("n", "<leader>f", function()
    vim.lsp.buf.format({ async = true })
end, { desc = "Format current buffer." })

vim.keymap.set("x", "<leader>f", function()
    vim.lsp.buf.format({ async = true })
end, { desc = "Format selection inside current buffer." })

vim.keymap.set("n", "<leader>l", "<cmd>cnext<CR>zz", { desc = "Jump to next error." })
vim.keymap.set("n", "<leader>h", "<cmd>cprev<CR>zz", { desc = "Jump to previous error." })
vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz", { desc = "Jump to next location." })
vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz", { desc = "Jump to previous location." })

vim.keymap.set("n", "<leader>s", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>",
    { desc = "Replace all occurances of specified text with new text." })
vim.keymap.set("v", "<leader>s", ":s///gI<Left><Left><Left><Left>",
    { desc = "Replace all occurances of specified text with new text." })

vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true }, { desc = "Make current file executable." })
vim.api.nvim_set_keymap('t', '<C-Esc>', '<C-\\><C-n>',
    { noremap = true, desc = "Return to normal mode in terminal." })

vim.keymap.set("n", '<F5>', '<cmd>make<CR>', { desc = "Run make command." })

-- buf navigation
vim.keymap.set("n", "<leader>.", "<cmd>bn<CR>", { desc = "Go to next buffer." })
vim.keymap.set("n", "<leader>,", "<cmd>bp<CR>", { desc = "Go to previous buffer." })

-- could be remapped to worse keys if n would be used for something else
vim.keymap.set("n", "<leader>n", "<cmd>noh<CR>", { desc = "Clear search highlights." })
