function SetupTheme(theme)
    theme = theme or "monokai-pro-machine"
    -- theme = theme or "rose-pine-moon"
    vim.cmd.colorscheme(theme)
end

SetupTheme()
