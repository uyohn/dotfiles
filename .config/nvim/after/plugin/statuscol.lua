local builtin = require("statuscol.builtin")

require('statuscol').setup({
    relculright = true,
    segments = {
        -- folds
        {
            text = { builtin.foldfunc },
            click = "v:lua.ScFa",
        },
        -- spacing
        { text = { " " },                   click = "v:lua.ScLa" },
        -- signs: gitsigns, lsp, etc.
        {
            sign = { name = { ".*" }, maxwidth = 2, colwidth = 1, auto = true },
            text = { "%s" },
            click = "v:lua.ScSa"
        },
        -- line numbers
        { text = { builtin.lnumfunc, " " }, click = "v:lua.ScLa" }
    }
})
