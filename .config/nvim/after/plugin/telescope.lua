require('telescope').load_extension('dap')
require('telescope').setup {
    pickers = {
        find_files = {
            theme = 'dropdown',
            previewer = false,
            find_command = { 'rg', '--files', '--hidden', '-g', '!.git' }
        },
        buffers = {
            theme = 'dropdown',
            previewer = false,
        },
    },
}

local builtin = require('telescope.builtin')

vim.keymap.set('n', '<C-p>', builtin.find_files, { desc = "Find files by fuzzy path" })
vim.keymap.set('n', '<C-]>', builtin.buffers, { desc = "Show buffers" })
--vim.keymap.set('n', '<C-p>', builtin.git_files, {})

vim.keymap.set('n', '<leader>tk', builtin.keymaps, { desc = "Show keymaps" })
vim.keymap.set('n', '<leader>tf', builtin.live_grep, { desc = "Search for files by content" })
vim.keymap.set('n', '<leader>tt', ":Telescope live_grep<CR>TODO:", { desc = "Search for TODOs in project" })

vim.keymap.set('n', '<leader>ps', function()
    builtin.grep_string({ search = vim.fn.input("Grep > ") });
end)
