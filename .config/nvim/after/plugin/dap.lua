-- mason dap config
require("mason-nvim-dap").setup({
    ensure_installed = { 'codelldb', 'js-debug-adapter' },
    handlers = {}
})

local dap, dapui = require("dap"), require("dapui")

-- dap
vim.keymap.set("n", "<S-F8>", function() dap.continue() end, { desc = "DAP Launch debug session / resume execution" })
vim.keymap.set("n", "<S-F9>", function() dap.step_into() end, { desc = "DAP Step into" })
vim.keymap.set("n", "<S-F7>", function() dap.step_out() end, { desc = "DAP Step out" })
vim.keymap.set("n", "<S-F10>", function() dap.step_over() end, { desc = "DAP Step over" })

vim.keymap.set("n", "<leader>db", function() dap.toggle_breakpoint() end, { desc = "DAP Set breakpoint" })
vim.keymap.set("n", "<leader>dc", function() dap.set_breakpoint(vim.fn.input('Breakpoint condition: ')) end,
    { desc = "DAP Set breakpoint with condition" })
vim.keymap.set("n", "<leader>dl", function() dap.set_breakpoint(nil, nil, vim.fn.input('Breakpoint log message: ')) end,
    { desc = "DAP Set breakpoint with log message" })
vim.keymap.set("n", "<leader>dr", function() dap.repl.open() end, { desc = "DAP Open a REPL / Debug-console" })

-- C/C++ debugging.
-- TODO: figure out why mason-nvim-dap setup doesn't work
dap.adapters.codelldb = {
    type = 'server',
    port = '49150',
    executable = {
        command = 'codelldb',
        args = { '--port', '49150' },
    }
}


-- signs
vim.api.nvim_set_hl(0, 'DapBreakpoint', { fg = '#ff6188' })
vim.api.nvim_set_hl(0, 'DapBreakpointCondition', { fg = '#ab9df2' })
vim.api.nvim_set_hl(0, 'DapLogPoint', { fg = '#78dce8' })
vim.api.nvim_set_hl(0, 'DapStopped', { fg = '#fc9867' })

vim.fn.sign_define('DapBreakpoint', {
    text = '',
    texthl = 'DapBreakpoint',
})
vim.fn.sign_define('DapBreakpointCondition',
    { text = '', texthl = 'DapBreakpointCondition' })
vim.fn.sign_define('DapBreakpointRejected',
    { text = '', texthl = 'DapBreakpoint' })
vim.fn.sign_define('DapLogPoint', { text = '◉', texthl = 'DapLogPoint' })
vim.fn.sign_define('DapStopped', { text = '󰏧', texthl = 'DapStopped' })

-- dapui
dapui.setup()
dap.listeners.before.attach.dapui_config = function()
    dapui.open()
end
dap.listeners.before.launch.dapui_config = function()
    dapui.open()
end
dap.listeners.before.event_terminated.dapui_config = function()
    dapui.close()
end
dap.listeners.before.event_exited.dapui_config = function()
    dapui.close()
end

vim.keymap.set("n", "<leader>du", function() dapui.toggle() end, { desc = "DAP UI Toggle" })

-- virtual text
require("nvim-dap-virtual-text").setup()
