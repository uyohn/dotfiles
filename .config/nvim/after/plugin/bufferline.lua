local bufferline = require("bufferline")

bufferline.setup {
    options = {
        separator_style = "thin",
        diagnostics = "nvim_lsp",
        offsets = {
            {
                filetype = "neo-tree",
                text = "Project Files",
                text_align = "left",
                separator = false
            }
        },
        right_mouse_command = "vert belowright sb %d",
        always_show_bufferline = false,
        custom_filter = function(buf_number)
            if vim.bo[buf_number].filetype ~= "fugitive" then
                return true
            end
        end,
    }
}
