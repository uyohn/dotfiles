vim.keymap.set("n", "<leader>q", ":Bdelete<CR>", { desc = "Delete current buffer." })
vim.keymap.set("n", "<leader>bd", vim.cmd.bdelete, { desc = "Delete current buffer." })
