# Dotfiles

Manage my dotfiles using `GNU Stow`. This repo contains branches for `arch` and `macos` systems. The `main` branch  
contains the common dotfiles for both systems.

## Deps
- [ ] `git`, `rg`, `fzf`, `fd`, `nodejs`, `npm` (`nvm`)
- [ ] `gnu stow`
- [ ] `zsh` and `oh-my-zsh`: + plugins `zsh-syntax-highlighting`, `zsh-autosuggestions`
- [ ] `neovim` + `packer.nvim`

### Optional
- [ ] `tmux`
- [ ] other cmd utilities (`exa`, etc.)

## Install

1. clone this repo into `$HOME/dotfiles`
1. cd into the repo
1. select preffered branch (e.g. `arch`)
1. run `git submodule update --init --recursive` to fetch all submodules (e.g. syntax highlighting for zsh)
1. run `stow .` to symlink all the dotfiles

