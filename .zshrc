export PATH=$HOME/bin:$HOME/.scripts:/usr/local/bin:/Users/uyohn/Library/Python/3.8/bin:$PATH
export ZSH="$HOME/.config/oh-my-zsh"

ZSH_THEME="uyohn"
ZSH_CUSTOM=~/.config/oh-my-zsh/custom/

plugins=(git zsh-syntax-highlighting zsh-autosuggestions)
source $ZSH/oh-my-zsh.sh

if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

alias vim="nvim"

alias KAFKA_BIN="/opt/homebrew/opt/kafka/bin/"

alias CHG="cd /Users/uyohn/Devel/CHG"
alias RDL="cd /Users/uyohn/Devel/RastockyDevLab/"

export NVM_DIR=~/.nvm

# For CHG GitHub repos access
export NPM_TOKEN="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"

# JAVA (for Kafka)
#export JAVA_HOME="/opt/homebrew/opt/openjdk/bin/"
export JAVA_HOME="/opt/homebrew/opt/openjdk"


## N4J
alias create-neo4j-container="~/.scripts/create-neo4j-container"
alias rollback-neo4j-data="~/.scripts/rollback-neo4j-changes"
export PATH="/opt/homebrew/opt/libpq/bin:$PATH"

## Bump CHG repo version
alias npmbump="npm version --no-git-tag-version patch"
alias awssso="aws sso login"
alias awseks="aws eks update-kubeconfig --name chg-platform-nonprod-1-21"

# bun completions
[ -s "/Users/uyohn/.bun/_bun" ] && source "/Users/uyohn/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"


# tmux
alias tns="tmux new -s "
alias tas="tmux attach -t "
alias tls="tmux list-sessions"

# llvm?
export LDFLAGS="-L/opt/homebrew/opt/llvm/lib"
export CPPFLAGS="-I/opt/homebrew/opt/llvm/include"

# TODO: find a better solution for os-specific things
if [[ "$(uname)" == "Darwin" ]]; then # MacOS
    source $(brew --prefix nvm)/nvm.sh

    # fix ICU4C pkg-config path
    export PKG_CONFIG_PATH=/opt/homebrew/opt/icu4c/lib/pkgconfig:$PKG_CONFIG_PATH
elif [[ "$(uname)" == "Linux" ]]; then # Linux
    source /usr/share/nvm/init-nvm.sh
fi

